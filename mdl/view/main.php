<?php

	class mainView{
		
		public function saludar(){
			template()->buildFromTemplates('template.html');
			template()->addTemplateBit('application_content','saludo.html');
			
			page()->setTitle('Inicio');
			
			template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}
		
		public function cuentas($cache){
			template()->buildFromTemplates('template.html');
			template()->addTemplateBit('application_content','cuentas.html');
			
			page()->setTitle('Catálogo de cuentas');
			page()->addEstigma('cuentas', array('SQL', $cache[0]));
			page()->addEstigma('cuentas2', array('SQL', $cache[1]));
			page()->addEstigma('s_1', "DEBE");
			page()->addEstigma('s_2', "HABER");
				
			template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}
		
		public function proveedores($cache){
			template()->buildFromTemplates('template.html');
			template()->addTemplateBit('application_content','proveedores.html');
			
			page()->setTitle('Catálogo de proveedores');
				
			template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}
	}
	
?>