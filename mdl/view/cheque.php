<?php

/**
* 
*/
class chequeView
{
	public function nuevo($cache){
		template()->buildFromTemplates('template.html');
		template()->addTemplateBit('application_content','cheque/nuevo.html');	
		page()->setTitle('Nuevo cheque');
		page()->addEstigma('bancos', array('SQL', $cache["bancos"]));
		template()->parseOutput();
		template()->parseExtras();
		print page()->getContent();
	}

	public function buscar(){
		template()->buildFromTemplates('template.html');
		template()->addTemplateBit('application_content','cheque/buscar.html');	
		page()->setTitle('Buscar cheque');
			
		template()->parseOutput();
		template()->parseExtras();
		print page()->getContent();
	}

	public function editar($nocheque, $cache){
		template()->buildFromTemplates('template.html');
		template()->addTemplateBit('application_content','cheque/editar.html');	
		page()->setTitle('Editar cheque');
		page()->addEstigma('chequeInfo', array('SQL', $cache));
		page()->addEstigma('noCheque', $nocheque);
		template()->parseOutput();
		template()->parseExtras();
		print page()->getContent();
	}
}

?>