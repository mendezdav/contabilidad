<?php	
	class testView {
		
		public function resourceTest(){
			template()->buildFromTemplates('template.html');
			template()->addTemplateBit('application_content','application_test.html');
			
			page()->setTitle('Application Test');
			
			template()->parseOutput();
			template()->parseExtras();
			print page()->getContent();
		}
		
	}	
?>