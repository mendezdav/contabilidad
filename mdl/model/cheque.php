<?php

/**
* 
*/
class chequeModel extends object
{
	
	function __construct()
	{
		# code...
	}

	public function guardar($banco, $no, $fecha, $beneficiario, $concepto, $monto){

		$stm = data_model()->getActiveConnection()->prepare("INSERT INTO contabilidad.cheque (Banco, NoCheque, Fecha, Beneficiario, Concepto, Monto) VALUES (?, ?, ?, ?, ?, ?)");
		$stm->bind_param("sssisd", $banco, $no, $fecha, $beneficiario, $concepto, $monto);
		$stm->execute();
		$stm->close();

		echo json_encode(array("msg"=>""));
	}

	public function actualizar($banco, $no, $fecha, $beneficiario, $concepto, $monto){

		$stm = data_model()->getActiveConnection()->prepare("UPDATE contabilidad.cheque SET Banco =?, Fecha=?, Beneficiario=?, Concepto=?, Monto=? WHERE NoCheque =?");
		$stm->bind_param("ssisds", $banco, $fecha, $beneficiario, $concepto, $monto, $no);
		$stm->execute();
		$stm->close();

		echo json_encode(array("msg"=>""));
	}
}

?>