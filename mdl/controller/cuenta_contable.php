<?php
	
	import('mdl.model.cuenta_contable');
	
	class cuenta_contableController extends controller{
		
		public function __construct(){
			$this->model = Helper::get_model($this);
		}
		
		public function guardar(){
			if(isset($_POST)&&!empty($_POST)){
				if($this->validarCampo('codigo_cuenta') && $this->validarCampo('nombre_cuenta') && $this->validarCampo('tipo_suma')){
					
					if(!$this->model->exists($_POST['codigo_cuenta'])){
						$this->model->get(0);
						$this->model->codigo = $_POST['codigo_cuenta'];
						$this->model->creada_el = date("Y-m-d");
					}else{
						$this->model->get($_POST['codigo_cuenta']);	
					}
					
					$this->model->nombre =	$_POST['nombre_cuenta'];
					$this->model->suma_en = $_POST['tipo_suma'];
					$this->model->madre = ($_POST['m_cuenta']!=-1)?$_POST['m_cuenta']:null;
					$this->model->save();
					HttpHandler::redirect('/contabilidad/main/cuentas?success=true');
				}else{
					HttpHandler::redirect('/contabilidad/main/cuentas?error=missing');
				}
			}
		}
		
		public function eliminar(){
			if(isset($_POST)&&!empty($_POST)){
				if($this->validarCampo('codigo')){
					$codigo = $_POST['codigo'];
					if($this->model->exists($codigo)){
						$query = "SELECT * FROM cuenta_contable WHERE madre = '{$codigo}'";
						data_model()->executeQuery($query);
						if(data_model()->getNumRows()>0){
							HttpHandler::redirect('/contabilidad/main/cuentas?error=dependency');
						}else{
							$this->model->delete($codigo);
							HttpHandler::redirect('/contabilidad/main/cuentas?success=deleted');	
						}
					}
				}
			}
		}
		
		public function cargar(){
			if(isset($_POST)&&!empty($_POST)){
				if($this->validarCampo('codigo')){
					$codigo = $_POST['codigo'];
					$response = array();
					$response['exists'] = false;
					
					if($this->model->exists($codigo)){
						$response['exists'] = true;
						$this->model->get($codigo);
						foreach($this->model->get_fields() as $field)
						$response[strtoupper($field)] = $this->model->$field;
					}
					
					echo json_encode($response);
				}	
			}
		}
		
		private function validarCampo($nombre_campo){
			return (isset($_POST[$nombre_campo]) && !empty($_POST[$nombre_campo]));
		}
	}
		
?>