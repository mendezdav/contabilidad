<?php

	import('mdl.model.main');
	import('mdl.view.main');

	class mainController extends controller{
		
		public function inicio(){		
			$this->view->saludar();
		}
		
		public function cuentas(){	
			$cache = array();
			$cache[0] = $this->model->get_sibling('cuenta_contable')->get_list();
			$cache[1] = $this->model->get_sibling('cuenta_contable')->get_list();	
			$this->view->cuentas($cache);
		}
		
		public function proveedores(){	
			$cache = array();	
			$this->view->proveedores($cache);
		}
	}
	
?>