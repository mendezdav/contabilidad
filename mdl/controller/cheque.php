<?php 


import('mdl.model.cheque');
import('mdl.view.cheque');

class chequeController extends controller
{
	public function nuevo(){
		$cache = array();
		$cache["bancos"] = data_model()->cacheQuery("SELECT * FROM casascredito");
		$this->view->nuevo($cache);
	}

	public function buscar(){

		$this->view->buscar();
	}

	public function listar(){
		$response = array();

		$query = "SELECT * FROM contabilidad.cheque";

		data_model()->executeQuery($query);

		while($row = data_model()->getResult()->fetch_assoc()){
			$response[] = $row;
		}

		echo json_encode($response);
	}

	public function editar(){

		if(isset($_GET['nocheque']) && !empty($_GET['nocheque'])){
			$nocheque = $_GET['nocheque'];
			$cacheCheque = data_model()->cacheQuery("SELECT * FROM contabilidad.cheque WHERE NoCheque='$nocheque'");
			$this->view->editar($nocheque, $cacheCheque);
		}
	}

	public function guardar(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		
		$banco = $request->banco;
		$no = $request->no;
		$fecha = $request->fecha;
		$beneficiario = $request->beneficiario;
		$concepto = $request->concepto;
		$monto = $request->monto;

		$this->model->guardar($banco, $no, $fecha, $beneficiario, $concepto, $monto);
	}

	public function actualizar(){
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		
		$banco = $request->banco;
		$no = $request->no;
		$fecha = $request->fecha;
		$beneficiario = $request->beneficiario;
		$concepto = $request->concepto;
		$monto = $request->monto;

		$this->model->actualizar($banco, $no, $fecha, $beneficiario, $concepto, $monto);
	}
}

?>