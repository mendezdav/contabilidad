(function(){
	'use strict';

	angular.module('contabilidadApp')
		.controller('chequeController', chequeController);

	chequeController.$inject = ['$http'];

	function chequeController($http){

		var vm = this;

		vm.guardarCheque = guardarCheque;
		vm.editarCheque = editarCheque;
		vm.actualizarCheque = actualizarCheque;

		vm.filtros = {
			NoCheque: {min:'000000', max:'999999'},
			Fecha: {min:'1980/01/01', max: '2025/12/31'},
			Concepto: '',
			Beneficiario: '',
			Banco: ''
		};

		vm.cheques = [];

		function guardarCheque(){
			$http.post('/contabilidad/cheque/guardar', vm.cheque).success(function(){
				alert("Se ha creado un nuevo cheque");
				vm.cheque = {
					banco:"",
					no: "",
					fecha:"",
					beneficiario:"",
					concepto:"",
					monto:0.0
				};
			});
		}

		function actualizarCheque(){
			$http.post('/contabilidad/cheque/actualizar', vm.cheque).success(function(){
				alert("Se ha actualizado un cheque");
				location.href = '/contabilidad/cheque/nuevo';
			});
		}

		function editarCheque(){
			var noCheque = prompt("Ingrese el número de cheque: ");

			if(noCheque!=null && noCheque.trim()!="" && !isNaN(noCheque.trim()) && parseInt(noCheque)>0){

				location.href = "/contabilidad/cheque/editar?nocheque="+noCheque;

			}else{

				alert("Número de cheque no válido");
			}
		}

		function cargarCheques(){
			$http.post('/contabilidad/cheque/listar', vm.cheque).success(function(response){
				vm.cheques = response;
			});
		}

		cargarCheques();

	}
})();